package kg.attractor.microgram2.dto;

import kg.attractor.microgram2.model.Publication;
import kg.attractor.microgram2.model.Subscribes;
import kg.attractor.microgram2.model.User;
import lombok.*;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class UserDTO {
    public static UserDTO from(User user) {
        return builder()
                .id(user.getId())
                .email(user.getEmail())
                .name(user.getName())
                .password(user.getPassword())
                .userPublications(user.getUserPublications())
                .thisUserSubscribers(user.getThisUserSubscribers())
                .thisUserSubscribes(user.getThisUserSubscribes())
                .build();
    }

    private String id = null;
    private String email;
    private String name;
    private String password;
    @Builder.Default
    private List<Publication> userPublications = new ArrayList<>();
    @Builder.Default
    private List<User> thisUserSubscribes = new ArrayList<>();
    @Builder.Default
    private List<Subscribes> thisUserSubscribers = new ArrayList<>();

}
