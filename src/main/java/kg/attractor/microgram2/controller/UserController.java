package kg.attractor.microgram2.controller;

import kg.attractor.microgram2.dto.PublicationDTO;
import kg.attractor.microgram2.dto.UserDTO;
import kg.attractor.microgram2.service.PublicationService;
import kg.attractor.microgram2.service.UserService;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    private final UserService userService;
    private final PublicationService userPublications;

    public UserController(UserService userService, PublicationService userPublications) {
        this.userService = userService;
        this.userPublications =  userPublications;
    }

   @PostMapping("/register")
   public UserDTO registerUser(@RequestBody UserDTO userData){
       return userService.addUser(userData);
   }
    @GetMapping("/profile/{userId}")
    public List<PublicationDTO> getProfile(@PathVariable String userId){
        return userPublications.getPublications(userId);
    }
    @DeleteMapping("/profile/{userId}")
    public ResponseEntity<Void> deleteUser(@PathVariable String userId) {
        if (userService.deleteUser(userId))
            return ResponseEntity.noContent().build();
        return ResponseEntity.notFound().build();
    }
}