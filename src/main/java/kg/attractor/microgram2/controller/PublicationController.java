package kg.attractor.microgram2.controller;
import kg.attractor.microgram2.dto.PublicationDTO;
import kg.attractor.microgram2.service.PublicationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/publication")
public class PublicationController {
    private final PublicationService publicationService;
    public PublicationController(PublicationService publicationService){
        this.publicationService = publicationService;
    }

    @PostMapping("/{userId}")
    public PublicationDTO addPublication(@RequestBody PublicationDTO publicationData, @PathVariable String userId) {
        return publicationService.addPublication(publicationData, userId);
    }

    @DeleteMapping("/{userId}/{publicationId}")
    public ResponseEntity<Void> deleteReview(@PathVariable String publicationId, @PathVariable String userId) {
        if (publicationService.deletePublication(publicationId, userId))
            return ResponseEntity.noContent().build();
        return ResponseEntity.notFound().build();
    }
}
