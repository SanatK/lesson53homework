package kg.attractor.microgram2.util;
import kg.attractor.microgram2.model.*;
import kg.attractor.microgram2.repository.CommentRepository;
import kg.attractor.microgram2.repository.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDateTime;
import java.util.Random;
import java.util.stream.Stream;


@Configuration
public class PreloadDatabaseWithData {

public final UserRepository ur;
public final CommentRepository cr;



public  PreloadDatabaseWithData(UserRepository ur, CommentRepository cr){
    this.ur = ur;
    this.cr = cr;
}
}
