package kg.attractor.microgram2.service;

import kg.attractor.microgram2.repository.CommentRepository;
import kg.attractor.microgram2.repository.PublicationRepository;
import kg.attractor.microgram2.repository.UserRepository;
import org.springframework.stereotype.Service;

@Service
public class CommentService {
    private final UserRepository userRepository;
    private final PublicationRepository publicationRepository;
    private final CommentRepository commentRepository;
    public CommentService(UserRepository userRepository, PublicationRepository publicationRepository, CommentRepository commentRepository) {
        this.userRepository = userRepository;
        this.publicationRepository = publicationRepository;
        this.commentRepository = commentRepository;
    }
}
