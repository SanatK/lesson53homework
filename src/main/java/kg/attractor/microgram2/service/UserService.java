package kg.attractor.microgram2.service;

import kg.attractor.microgram2.dto.PublicationDTO;
import kg.attractor.microgram2.dto.UserDTO;
import kg.attractor.microgram2.model.Publication;
import kg.attractor.microgram2.model.User;
import kg.attractor.microgram2.repository.CommentRepository;
import kg.attractor.microgram2.repository.LikeRepository;
import kg.attractor.microgram2.repository.PublicationRepository;
import kg.attractor.microgram2.repository.UserRepository;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Service
public class UserService {
    private final UserRepository userRepository;
    private final PublicationRepository publicationRepository;
    private final CommentRepository commentRepository;
    public UserService(UserRepository userRepository, PublicationRepository publicationRepository, CommentRepository commentRepository) {
        this.userRepository = userRepository;
        this.publicationRepository = publicationRepository;
        this.commentRepository = commentRepository;
    }
    public UserDTO addUser(UserDTO userData){
        var user = User.builder()
                .id(userData.getId())
                .email(userData.getEmail())
                .name(userData.getName())
                .password(userData.getPassword())
                .userPublications(userData.getUserPublications())
                .build();
        userRepository.save(user);
        return UserDTO.from(user);
    }
    public boolean deleteUser(String userId){
        userRepository.deleteById(userId);
        return true;
    }
}
