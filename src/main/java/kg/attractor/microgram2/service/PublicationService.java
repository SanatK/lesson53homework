package kg.attractor.microgram2.service;

import kg.attractor.microgram2.dto.PublicationDTO;
import kg.attractor.microgram2.dto.UserDTO;
import kg.attractor.microgram2.exception.ResourceNotFoundException;
import kg.attractor.microgram2.model.Publication;
import kg.attractor.microgram2.model.User;
import kg.attractor.microgram2.repository.CommentRepository;
import kg.attractor.microgram2.repository.PublicationImageRepository;
import kg.attractor.microgram2.repository.PublicationRepository;
import kg.attractor.microgram2.repository.UserRepository;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PublicationService {
    private final UserRepository userRepository;
    private final PublicationRepository publicationRepository;
    private final CommentRepository commentRepository;
    private final PublicationImageRepository publicationImageRepository;

    public PublicationService(UserRepository userRepository, PublicationRepository publicationRepository, CommentRepository commentRepository, PublicationImageRepository publicationImageRepository) {
        this.userRepository = userRepository;
        this.publicationRepository = publicationRepository;
        this.commentRepository = commentRepository;
        this.publicationImageRepository = publicationImageRepository;
    }
    public List<PublicationDTO> getPublications(String userId){
        List<Publication> publications = publicationRepository.findAllByUserId(userId);
        List<PublicationDTO> publicationDTO = new ArrayList<>();
        for(Publication p: publications){
            PublicationDTO pDTO = PublicationDTO.from(p);
            publicationDTO.add(pDTO);
        }
        return publicationDTO;
    }
    public PublicationDTO addPublication(PublicationDTO publicationData, String userId){
        var publicationImage = publicationImageRepository.findById(publicationData.getImageId())
                .orElseThrow(() -> new ResourceNotFoundException("Movie Image with " + publicationData.getImageId() + " doesn't exists!"));
        var publication = Publication.builder()
                .id(publicationData.getId())
                .description(publicationData.getDescription())
                .publicationTime(LocalDateTime.now())
                .image(publicationImage)
                .build();
        publicationRepository.save(publication);
        userRepository.getUserById(userId).getUserPublications().add(publication);
        //при попытке записать публикации юзеру выдает NullPointerException, не могу разобраться
        //буду очень признателен если опишите в чем проблема
        //помню мы как то решали это через конструктор, но не смог сделать
        return PublicationDTO.from(publication);
    }
    public boolean deletePublication(String publicationId, String userId) {
        publicationRepository.deleteById(publicationId);
        userRepository.getUserById(userId).getUserPublications()
                .stream()
                .filter(publication -> !publication.getId().equals(publicationId))
                .collect(Collectors.toList());
        return true;
    }
}
