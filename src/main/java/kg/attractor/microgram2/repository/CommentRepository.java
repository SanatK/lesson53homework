package kg.attractor.microgram2.repository;

import kg.attractor.microgram2.model.Comment;
import org.springframework.data.repository.CrudRepository;

public interface CommentRepository extends CrudRepository<Comment, String> {

}
