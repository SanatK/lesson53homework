package kg.attractor.microgram2.repository;

import kg.attractor.microgram2.model.Like;
import org.springframework.data.repository.CrudRepository;

public interface LikeRepository extends CrudRepository<Like, String> {
    //Найти посты с лайками юзера по юзеру
}
