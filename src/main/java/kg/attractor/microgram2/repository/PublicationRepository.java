package kg.attractor.microgram2.repository;

import kg.attractor.microgram2.model.Publication;
import kg.attractor.microgram2.model.User;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PublicationRepository extends CrudRepository<Publication, String> {
    @Query("{ 'user.id' : { $regex : '?0', $options : 'i' } } ")
    List<Publication> findAllByUserId(String userId);
    public void deleteById(String id);
}
