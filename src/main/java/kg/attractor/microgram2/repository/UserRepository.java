package kg.attractor.microgram2.repository;

import kg.attractor.microgram2.model.Publication;
import kg.attractor.microgram2.model.User;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepository extends CrudRepository<User, String> {
    public User findByName(String name);
    public User findByPassword(String password);
    @Query("{ 'user.id' : { $regex : '?0', $options : 'i' } } ")
    User getUserById(String id);
}
