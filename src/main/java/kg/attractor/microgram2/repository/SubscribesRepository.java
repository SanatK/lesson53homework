package kg.attractor.microgram2.repository;

import kg.attractor.microgram2.model.Subscribes;
import org.springframework.data.repository.CrudRepository;

public interface SubscribesRepository extends CrudRepository<Subscribes, String> {
}
