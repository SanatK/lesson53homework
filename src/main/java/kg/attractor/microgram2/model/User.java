package kg.attractor.microgram2.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
@Document(collection="users")
public class User {
    @Id
    @Builder.Default
    private String id = UUID.randomUUID().toString();
    private String email;
    private String name;
    private String password;
    @DBRef
    private List<Publication> userPublications = new ArrayList<>();
    private List<User> thisUserSubscribes = new ArrayList<>();
    @DBRef
    private List<Subscribes> thisUserSubscribers = new ArrayList<>();
}
