package kg.attractor.microgram2.model;

        import lombok.*;
        import org.springframework.data.annotation.Id;
        import org.springframework.data.mongodb.core.index.Indexed;
        import org.springframework.data.mongodb.core.mapping.DBRef;
        import org.springframework.data.mongodb.core.mapping.Document;

        import java.time.LocalDateTime;
        import java.util.ArrayList;
        import java.util.List;
        import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
@Document(collection="publications")
public class Publication {
    @Id
    @Builder.Default
    private String id = UUID.randomUUID().toString();
    private String description;
    private LocalDateTime publicationTime;
    @DBRef
    private User user;
    @DBRef
    private PublicationImage image;
    @DBRef
    @Builder.Default
    private List<Comment> comments  = new ArrayList<>();
    @Builder.Default
    private List<Like> likes = new ArrayList<>();
    // I think rating should be calculated upon adding a review and updated in the document
    // think service layer or even better move to separate consumer to calculate and update (queues)
}
