package kg.attractor.microgram2.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import java.time.LocalDateTime;
import java.util.UUID;
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
@Document(collection = "likes")
public class Like {
    @Builder.Default
    private String id = UUID.randomUUID().toString();
    private User user;
    @Indexed
    private LocalDateTime likeTime;
}
